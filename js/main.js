$('.closeX').on('click',function(){
    $('.invitation-register').fadeOut(100);
    $('.call-register').fadeIn(100);
});

$('.call-register').on('click',function(){
    $('.invitation-register').fadeIn(100);
    $('.call-register').fadeOut(100);
});

//owl-carousel 1

$(document).ready(function () {
    $("#owl-example1").owlCarousel({
        margin: 5,
        loop: true,
        callbacks: true,
        items: 1,
        slideSpeed: 500,
        autoplay: true,
        nav: false,
        autoPlaySpeed: 500,
        smartSpeed: 500,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
                loop: true
            }
        }
    });
});

//owl-carousel 2

$(document).ready(function () {
    $("#owl-example2").owlCarousel({
        margin: 5,
        loop: true,
        callbacks: true,
        items: 1,
        slideSpeed: 500,
        autoplay: true,
        nav: false,
        autoPlaySpeed: 500,
        smartSpeed: 500,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
                loop: true
            }
        }
    });
});

//school-selected event








